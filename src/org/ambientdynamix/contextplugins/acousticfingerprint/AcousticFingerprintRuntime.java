/*
 * Copyright (C) The Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ambientdynamix.contextplugins.acousticfingerprint;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;

import org.ambientdynamix.api.application.ErrorCodes;
import org.ambientdynamix.api.contextplugin.ContextPluginSettings;
import org.ambientdynamix.api.contextplugin.PluginConstants;
import org.ambientdynamix.api.contextplugin.PowerScheme;
import org.ambientdynamix.api.contextplugin.ReactiveContextPluginRuntime;
import org.ambientdynamix.api.contextplugin.security.PrivacyRiskLevel;
import org.ambientdynamix.api.contextplugin.security.SecuredContextInfo;
import org.ambientdynamix.api.contextplugin.security.SecuredSensorManager;
import org.ambientdynamix.contextplugins.acousticfingerprint.AudioFingerprinter.AudioFingerprinterListener;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.provider.SyncStateContract.Constants;
import android.util.Log;

/**
 * Example acoustic fingerprint plug-in that identifies audio clips using the device's microphone. This plug-in is not
 * production ready; rather, it's used to demonstrate how natively compiled code can be deployed in a plug-in.
 * 
 * @author Darren Carlson
 * 
 */
public class AcousticFingerprintRuntime extends ReactiveContextPluginRuntime implements AudioFingerprinterListener {
	// Private data
	private final String TAG = this.getClass().getSimpleName();
	private AudioFingerprinter fingerprinter;
	private boolean fingerprinting;
	private List<UUID> requestIds = new ArrayList<UUID>();

	/*
	 * Loading native so files from Bundle JARs works 1. Compile the native code according to the NDK tutorial files 2.
	 * Update the manifest to use: Bundle-NativeCode: libs/armeabi/libhello-jni.so ; osname=Linux ; processor=arm_le
	 * (Problem with Eclipse export when using osname and processor - so you may need to export without and copy the
	 * manifest back) 3. Make sure that the libs/ folder is added to the build.properties 4. Export and copy classes.dex
	 * to the Bundle root as usual. Links: http://robertvarttinen.blogspot.de/2008/12/bundle-nativecode-in-osgi-manifest.html
	 * http://www.javacodegeeks.com/2012/05/learn-by-errors-java-osgi.html
	 * https://groups.google.com/forum/?fromgroups#!topic/android-ndk/u1v64AapyKI
	 * http://stackoverflow.com/questions/4882167
	 * /creating-a-product-sdk-how-do-i-add-a-native-lib-so-and-a-jar-with-the-sdk-i
	 * https://bugzilla.mozilla.org/show_bug.cgi?id=588607
	 */
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void init(PowerScheme powerScheme, ContextPluginSettings settings) throws Exception {
		// Set the power scheme
		this.setPowerScheme(powerScheme);
		// Create an AudioFingerprinter
		fingerprinter = new AudioFingerprinter(this);
		// Clear any existing requests
		requestIds.clear();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void start() {
		/*
		 * At this point, the plug-in should begin performing context interactions or waiting for context requests.
		 */
		Log.d(TAG, "Started!");
	}

	@Override
	public void stop() {
		/*
		 * At this point, the plug-in should stop performing context interactions, but retain resources needed to run again.
		 */
		Log.d(TAG, "Stopped!");
		if (fingerprinter != null)
			fingerprinter.stop();
		requestIds.clear();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void destroy() {
		/*
		 * At this point, the plug-in should release any resources.
		 */
		this.stop();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void handleConfiguredContextRequest(UUID arg0, String arg1, Bundle arg2) {

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void handleContextRequest(UUID requestId, String contextType) {
		synchronized (requestIds) {
			requestIds.add(requestId);
			if (!fingerprinting)
				fingerprinter.fingerprint(20);
			else
				Log.i(TAG, "Already fingerprinting... binding request to current task");
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void updateSettings(ContextPluginSettings settings) {
		// Not supported
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setPowerScheme(PowerScheme scheme) {
		// Not supported
	}
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void didFinishListening() {
		Log.i(TAG, "didFinishListening");
		fingerprinting = false;
	}

	@Override
	public void didFinishListeningPass() {
		Log.i(TAG, "didFinishListeningPass");
	}

	@Override
	public void willStartListening() {
		Log.i(TAG, "willStartListening");
	}

	@Override
	public void willStartListeningPass() {
		Log.i(TAG, "willStartListeningPass");
	}

	@Override
	public void didGenerateFingerprintCode(String code) {
		Log.i(TAG, "didGenerateFingerprintCode: " + code);
	}

	@Override
	public void didFindMatchForCode(List<ISongInfo> songs, String code) {
		Log.i(TAG, "didFindMatchForCode: " + code);
		for (ISongInfo s : songs) {
			Log.i(TAG, "MATCH: " + s + " - " + s);
		}
		synchronized (requestIds) {
			for (UUID responseId : requestIds) {
				sendContextEvent(responseId, new SecuredContextInfo(new SongCollection(songs), PrivacyRiskLevel.LOW,
						PluginConstants.JSON_WEB_ENCODING));
			}
			requestIds.clear();
		}
	}

	@Override
	public void didNotFindMatchForCode(String code) {
		Log.i(TAG, "didNotFindMatchForCode: " + code);
		synchronized (requestIds) {
			for (UUID responseId : requestIds) {
				sendContextRequestError(responseId, "Could not find song for audio", ErrorCodes.CONTEXT_SCAN_FAILED);
			}
			requestIds.clear();
		}
	}

	@Override
	public void didFailWithException(Exception e) {
		Log.i(TAG, "didFailWithException: " + e);
		synchronized (requestIds) {
			for (UUID responseId : requestIds) {
				sendContextRequestError(responseId, "Error during context scan: " + e.toString(),
						ErrorCodes.CONTEXT_SCAN_FAILED);
			}
			requestIds.clear();
		}
	}
}