/*
 * Copyright (C) The Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ambientdynamix.contextplugins.acousticfingerprint;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Represents data about a song.
 * 
 * @author Darren Carlson
 * 
 */
class SongInfo implements Parcelable, ISongInfo {
	/**
	 * Required CREATOR field that generates instances of this Parcelable class from a Parcel.
	 * 
	 * @see http://developer.android.com/reference/android/os/Parcelable.Creator.html
	 */
	public static Parcelable.Creator<SongInfo> CREATOR = new Parcelable.Creator<SongInfo>() {
		public SongInfo createFromParcel(Parcel in) {
			return new SongInfo(in);
		}

		public SongInfo[] newArray(int size) {
			return new SongInfo[size];
		}
	};
	// Private data
	private String title;
	private String artistName;
	private String artistId;
	private int score;
	private String songId;

	/**
	 * Creates a SongInfo
	 */
	public SongInfo() {
	}

	/**
	 * {@inheritDoc}
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * Sets the song's title.
	 */
	void setTitle(String title) {
		this.title = title;
	}

	/**
	 * {@inheritDoc}
	 */
	public String getArtistName() {
		return artistName;
	}

	/**
	 * Sets the song's artist name.
	 */
	void setArtistName(String artistName) {
		this.artistName = artistName;
	}

	/**
	 * {@inheritDoc}
	 */
	public String getArtistId() {
		return artistId;
	}

	/**
	 * Sets the song's artist id.
	 */
	void setArtistId(String artistId) {
		this.artistId = artistId;
	}

	/**
	 * {@inheritDoc}
	 */
	public int getScore() {
		return score;
	}

	/**
	 * Sets the song's score, representing the accuracy of the match.
	 */
	void setScore(int score) {
		this.score = score;
	}

	/**
	 * {@inheritDoc}
	 */
	public String getSongId() {
		return this.songId;
	}

	/**
	 * Sets the song's id.
	 */
	void setSongId(String sonId) {
		this.songId = sonId;
	}

	/**
	 * Used internally for handling Parcelable data.
	 */
	public int describeContents() {
		return 0;
	}

	/**
	 * Used internally for handling Parcelable data.
	 */
	public void writeToParcel(Parcel out, int flags) {
		out.writeString(this.title);
		out.writeString(this.artistName);
		out.writeString(this.title);
		out.writeInt(this.score);
		out.writeString(this.songId);
	}

	/**
	 * Used internally for handling Parcelable data.
	 */
	private SongInfo(final Parcel in) {
		this.title = in.readString();
		this.artistName = in.readString();
		this.artistId = in.readString();
		this.score = in.readInt();
		this.songId = in.readString();
	}

	@Override
	public String toString() {
		return "Song: Title = " + title + ", song id = " + songId + ", atrist = " + artistName + ", artist id = "
				+ artistId;
	}
}