/*
 * Copyright (C) The Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ambientdynamix.contextplugins.acousticfingerprint;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Represents a collection of ISongInfo.
 * 
 * @author Darren Carlson
 * 
 */
public class SongCollection implements ISongCollection {
	/**
	 * Required CREATOR field that generates instances of this Parcelable class from a Parcel.
	 * 
	 * @see http://developer.android.com/reference/android/os/Parcelable.Creator.html
	 */
	public static Parcelable.Creator<SongCollection> CREATOR = new Parcelable.Creator<SongCollection>() {
		public SongCollection createFromParcel(Parcel in) {
			return new SongCollection(in);
		}

		public SongCollection[] newArray(int size) {
			return new SongCollection[size];
		}
	};
	// Private data
	private List<ISongInfo> songs = new ArrayList<ISongInfo>();

	/**
	 * Creates a SongCollection.
	 */
	public SongCollection(List<ISongInfo> songs) {
		this.songs = songs;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getStringRepresentation(String format) {
		return "";
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getImplementingClassname() {
		return this.getClass().getName();
	}

	/*
	 * (non-Javadoc)
	 * @see org.ambientdynamix.contextplugins.acousticfingerprint.ISongCollection#getContextType()
	 */
	@Override
	public String getContextType() {
		return "org.ambientdynamix.contextplugins.acousticfingerprint.discoversongs";
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Set<String> getStringRepresentationFormats() {
		Set<String> formats = new HashSet<String>();
		formats.add("text/plain");
		return formats;
	};

	/**
	 * {@inheritDoc}
	 */
	public List<ISongInfo> getSongs() {
		return songs;
	}

	/**
	 * Used internally for handling Parcelable data.
	 */
	public int describeContents() {
		return 0;
	}

	/**
	 * Used internally for handling Parcelable data.
	 */
	public void writeToParcel(Parcel out, int flags) {
		out.writeList(songs);
	}

	/**
	 * Used internally for handling Parcelable data.
	 */
	private SongCollection(final Parcel in) {
		in.readList(this.songs, getClass().getClassLoader());
	}
}