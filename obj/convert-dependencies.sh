#!/bin/sh
# AUTO-GENERATED FILE, DO NOT EDIT!
if [ -f $1.org ]; then
  sed -e 's!^R:/dev/tools/cygwin/lib!/usr/lib!ig;s! R:/dev/tools/cygwin/lib! /usr/lib!ig;s!^R:/dev/tools/cygwin/bin!/usr/bin!ig;s! R:/dev/tools/cygwin/bin! /usr/bin!ig;s!^R:/dev/tools/cygwin/!/!ig;s! R:/dev/tools/cygwin/! /!ig;s!^S:!/cygdrive/s!ig;s! S:! /cygdrive/s!ig;s!^R:!/cygdrive/r!ig;s! R:! /cygdrive/r!ig;s!^Q:!/cygdrive/q!ig;s! Q:! /cygdrive/q!ig;s!^P:!/cygdrive/p!ig;s! P:! /cygdrive/p!ig;s!^D:!/cygdrive/d!ig;s! D:! /cygdrive/d!ig;s!^C:!/cygdrive/c!ig;s! C:! /cygdrive/c!ig;' $1.org > $1 && rm -f $1.org
fi
